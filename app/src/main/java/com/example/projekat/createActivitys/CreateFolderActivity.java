package com.example.projekat.createActivitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.example.projekat.R;
import com.example.projekat.model.Condition;
import com.example.projekat.model.Folder;
import com.example.projekat.model.Operation;
import com.example.projekat.model.Rule;
import com.example.projekat.sActivitys.EmailsActivity;
import com.example.projekat.service.FolderService;
import com.example.projekat.service.ServiceUtils;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.Toolbar;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateFolderActivity extends AppCompatActivity {
    private EditText txtName;
    private RadioGroup rgCondition;
    private RadioGroup rgOperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_folder);

        Toolbar toolbar = findViewById(R.id.toolbar_create_folder);
        setActionBar(toolbar);

        txtName = findViewById(R.id.folder_name);
        rgCondition = findViewById(R.id.rgCondition);
        rgOperation = findViewById(R.id.rgOperation);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_folder_menu, menu);
        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.saveFolder:
                String name = txtName.getText().toString().trim();

                RadioButton rbc = findViewById(rgCondition.getCheckedRadioButtonId());
                Condition c = Condition.getCondition(rbc.getText().toString());

                RadioButton rbo = findViewById(rgOperation.getCheckedRadioButtonId());
                Operation o = Operation.getOperation(rbo.getText().toString());

                if (name.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Folder name is empty!", Toast.LENGTH_LONG).show();
                }else{
                    Folder folder = new Folder();
                    folder.setName(name);
                    Rule r = new Rule();
                    r.setCondition(c);
                    r.setOperation(o);
//                    folder.getRules().add(r);

                    FolderService folderService = ServiceUtils.getRetrofitInstance().create(FolderService.class);
                    Call<RequestBody> call = folderService.addFolder(folder);
                    call.enqueue(new Callback<RequestBody>() {
                        @Override
                        public void onResponse(@NonNull Call<RequestBody> call, @NonNull Response<RequestBody> response) {
                            if(response.isSuccessful()){
                                Toast.makeText(CreateFolderActivity.this, R.string.save,Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), EmailsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else{
                                Toast.makeText(CreateFolderActivity.this, "Service error",Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<RequestBody> call, @NonNull Throwable t) {
                            Toast.makeText(CreateFolderActivity.this, "Service error",Toast.LENGTH_LONG).show();
                            System.out.println(t);
                        }
                    });
                }

                Toast.makeText(CreateFolderActivity.this, R.string.save,Toast.LENGTH_SHORT).show();
                break;

            case R.id.cancelFolder:
                super.onBackPressed();
        }
        return true;
    }
    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onRestart(){
        super.onRestart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }



}