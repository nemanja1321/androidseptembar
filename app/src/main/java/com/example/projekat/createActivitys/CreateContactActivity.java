package com.example.projekat.createActivitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.projekat.R;
import com.example.projekat.model.Contacts;
import com.example.projekat.model.Format;
import com.example.projekat.model.Photo;
import com.example.projekat.sActivitys.EmailsActivity;
import com.example.projekat.service.ContactsService;
import com.example.projekat.service.ServiceUtils;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateContactActivity extends AppCompatActivity {
    private EditText edtFirstName;
    private EditText edtLastName;
    private EditText edtEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        Toolbar toolbar = findViewById(R.id.toolbar_create_contact);
        setActionBar(toolbar);

        edtFirstName = findViewById(R.id.txtFName);
        edtLastName = findViewById(R.id.txtLName);
        edtEmail = findViewById(R.id.txtEmail);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_contact_menu ,menu);
        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.saveContact:
                Toast.makeText(CreateContactActivity.this, R.string.save,Toast.LENGTH_SHORT).show();
                String fn = edtFirstName.getText().toString().trim();
                String ln = edtLastName.getText().toString().trim();
                //ImageView dp = edtDisplay.getText().toString().trim();
                String em = edtEmail.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if(fn.equals("") || ln.equals("") || em.equals("")) {
                    Toast.makeText(CreateContactActivity.this, "Please fill in all required fields.", Toast.LENGTH_SHORT).show();

                }else if(!em.matches(emailPattern)) {
                    Toast.makeText(CreateContactActivity.this, "Invalid email address", Toast.LENGTH_SHORT).show();
                } else {
                    final Contacts contact = new Contacts(6, fn, ln, new Photo(0,"",null), em, Format.Plain, null, null, null,null);

                    ContactsService service = ServiceUtils.getRetrofitInstance().create(ContactsService.class);
                    Call<RequestBody> call = service.addContacts(contact);

                    call.enqueue(new Callback<RequestBody>() {
                        @Override
                        public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {
                            if(response.isSuccessful()){
                                Toast.makeText(CreateContactActivity.this, R.string.save,Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), EmailsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onFailure(Call<RequestBody> call, Throwable t) {
                            Toast.makeText(CreateContactActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                        }
                    });
                }


            case R.id.cancelContact:
                super.onBackPressed();
                break;
        }
        return true;
    }
    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onRestart(){
        super.onRestart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }
}