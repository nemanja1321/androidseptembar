package com.example.projekat.sActivitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.preference.PreferenceManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.projekat.R;
import com.example.projekat.activitys.LoginActivity;
import com.example.projekat.activitys.ProfileActivity;
import com.example.projekat.activitys.SettingsActivity;
import com.example.projekat.adapter.DrawerAdapter;
import com.example.projekat.adapter.EmailAdapter;
import com.example.projekat.createActivitys.CreateEmailActivity;
import com.example.projekat.model.Message;
import com.example.projekat.model.NavItem;
import com.example.projekat.service.MessageService;
import com.example.projekat.service.ServiceUtils;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailsActivity extends AppCompatActivity {


    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mTitle;
    private ArrayList<NavItem> mNavItem = new ArrayList<>();


    private Handler mHandler;
    private long mInterval = 0;
    private String username ;
    private String url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emails);
        Toolbar toolbar = findViewById(R.id.toolbar_email);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        mHandler = new Handler();

        FloatingActionButton floatingActionButton = findViewById(R.id.fabEmails);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmailsActivity.this, CreateEmailActivity.class);
                startActivity(intent);
            }
        });

        prepareMenu(mNavItem);


        mTitle = getTitle();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerList = findViewById(R.id.nav_list);
        ListView emailsView = findViewById(R.id.mainContentE);
        mDrawerPane = findViewById(R.id.drawer_pane);
        DrawerAdapter adapter = new DrawerAdapter(this, mNavItem);
        mDrawerLayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerList.setAdapter(adapter);
        emailsView.setOnItemClickListener(new ListItemClickLIstener());



        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setIcon(R.mipmap.ic_launcher);
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ) {

            public void onDrawerClosed(View view) {
                Objects.requireNonNull(getSupportActionBar()).setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                Objects.requireNonNull(getSupportActionBar()).setTitle("Projekat");
                invalidateOptionsMenu();
            }
        };


        SharedPreferences preferences = getSharedPreferences(LoginActivity.LOGGED_USER, Context.MODE_PRIVATE);
        username = preferences.getString(LoginActivity.USER_USERNAME, "");
        url = preferences.getString(LoginActivity.USER_IMG, "");

        RelativeLayout rlProfileBox = findViewById(R.id.profileBox);

        TextView txtUsername = rlProfileBox.findViewById(R.id.UserName);
        txtUsername.setText(username);

        ImageView imgAvatar = rlProfileBox.findViewById(R.id.avatar);
        if (url.trim().isEmpty()){
            Picasso.get().load(R.drawable.profile_image).into(imgAvatar);

        }else{
            Picasso.get().load(url).into(imgAvatar);
        }
        rlProfileBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmailsActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });


    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectItemFromDrawer(i);
        }
    }

    private static class ListItemClickLIstener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        }
    }

    private void selectItemFromDrawer(int position) {
        Intent intent;
        if (position == 0) {
            intent = new Intent(EmailsActivity.this, ContactsActivity.class);
            startActivity(intent);
        } else if (position == 1) {
            intent = new Intent(EmailsActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (position == 2) {
            intent = new Intent(EmailsActivity.this, FoldersActivity.class);
            startActivity(intent);
        } else if (position == 3) {
            intent = new Intent(EmailsActivity.this, SettingsActivity.class);
            startActivity(intent);
        } else if (position == 4) {
            SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.LOGGED_USER, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putString(LoginActivity.USER_PASSWORD, null);
            editor.putString(LoginActivity.USER_USERNAME, null);
            editor.apply();

            Intent l = new Intent(EmailsActivity.this, LoginActivity.class);
            l.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(l);
            finish();

        } else {
            Log.e("DRAWER", "Nesto van opsega");
        }
        mDrawerList.setItemChecked(position, true);
        if (position != 4) {
            setTitle(mNavItem.get(position).getmTitle());
        }
        mDrawerLayout.closeDrawer(mDrawerPane);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        Objects.requireNonNull(getSupportActionBar()).setTitle(mTitle);
    }




    private void prepareMenu(ArrayList<NavItem> mNavItem) {
        mNavItem.add(new NavItem(getString(R.string.contacts), getString(R.string.contacts_long), R.drawable.contacts));
        mNavItem.add(new NavItem(getString(R.string.profile), getString(R.string.profile_long), R.drawable.person));
        mNavItem.add(new NavItem(getString(R.string.folders), getString(R.string.folders_long), R.drawable.folders));
        mNavItem.add(new NavItem(getString(R.string.settings), getString(R.string.settings_long), R.drawable.settings));
        mNavItem.add(new NavItem(getString(R.string.logOut), getString(R.string.logoOut_long), R.mipmap.ic_action_about));

    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String syncTimeStr = sharedPreferences.getString("pref_sync_list", "0");

                mInterval = TimeUnit.MINUTES.toMillis(Integer.parseInt(syncTimeStr));
                boolean sort = Boolean.parseBoolean(sharedPreferences.getString("pref_sync_list", "true"));

                Toast.makeText(getApplicationContext(), "Syncing...", Toast.LENGTH_SHORT).show();

                MessageService messageService = ServiceUtils.getRetrofitInstance().create(MessageService.class);
                Call<ArrayList<Message>> call = messageService.getMessage(/*sort*/);
                call.enqueue(new Callback<ArrayList<Message>>() {
                    @Override
                    public void onResponse(@NonNull Call<ArrayList<Message>> call, @NonNull Response<ArrayList<Message>> response) {

                        ArrayList<Message> messages = response.body();
                        ListView listView = findViewById(R.id.mainContentE);
                        EmailAdapter adapter = new EmailAdapter(EmailsActivity.this, R.layout.email_list, messages);
                        listView.setAdapter(adapter);

                    }

                    @Override
                    public void onFailure(@NonNull Call<ArrayList<Message>> call, @NonNull Throwable t) {
                        //Toast.makeText(getApplicationContext(),"Error!", Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                        //System.out.println(t);
                    }
                });

            }finally {
                mHandler.postDelayed(mStatusChecker,mInterval);
            }
        }
    };

    void startRepeatingTask(){
        mStatusChecker.run();
    }

    void stopRepeatingTask(){
        mHandler.removeCallbacks(mStatusChecker);
    }





    @Override
    public void onResume(){
        super.onResume();
        startRepeatingTask();
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onRestart(){
        super.onRestart();
    }

    @Override
    public void onPause(){
        super.onPause();
        stopRepeatingTask();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

}
