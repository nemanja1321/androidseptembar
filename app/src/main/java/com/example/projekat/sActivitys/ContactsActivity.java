package com.example.projekat.sActivitys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import com.example.projekat.R;

import com.example.projekat.activitys.LoginActivity;
import com.example.projekat.activitys.ProfileActivity;
import com.example.projekat.activitys.SettingsActivity;

import com.example.projekat.adapter.ContactsAdapter;
import com.example.projekat.adapter.DrawerAdapter;
import com.example.projekat.createActivitys.CreateContactActivity;
import com.example.projekat.model.Contacts;
import com.example.projekat.model.NavItem;
import com.example.projekat.service.ContactsService;
import com.example.projekat.service.ServiceUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.preference.PreferenceManager;

import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactsActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ListView contactView;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mTitle;
    private ArrayList<NavItem> mNavItem = new ArrayList<>();
    private ArrayList<Contacts> mContacts = new ArrayList<>();
    private String username;
    private String url;
    private long mInterval;
    private Handler mHendler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        prepareMenu(mNavItem);

        mTitle = getTitle();
        mDrawerLayout = findViewById(R.id.DrawerLayout);
        mDrawerList = findViewById(R.id.navList);
        contactView = findViewById(R.id.mainContent);
        mDrawerPane = findViewById(R.id.DrawerPane);
        DrawerAdapter adapter = new DrawerAdapter(this, mNavItem);
        mDrawerLayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerList.setAdapter(adapter);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setIcon(R.mipmap.ic_launcher);
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
                ){

            public void onDrawerClosed(View view){
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
            public void onDrawerOpened(View drawerView){
                getSupportActionBar().setTitle("Projekat");
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        SharedPreferences preferences = getSharedPreferences(LoginActivity.LOGGED_USER, Context.MODE_PRIVATE);
        username = preferences.getString(LoginActivity.USER_USERNAME, "");
        url = preferences.getString(LoginActivity.USER_IMG, "");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactsActivity.this, CreateContactActivity.class);
                startActivity(intent);
            }
        });

        RelativeLayout rlProfileBox = findViewById(R.id.profileBox);

        TextView txtUsername = rlProfileBox.findViewById(R.id.UserName);
        txtUsername.setText(username);

        ImageView imgAvatar = rlProfileBox.findViewById(R.id.avatar);
        if (url.trim().isEmpty()){
            Picasso.get().load(R.drawable.profile_image).into(imgAvatar);

        }else{
            Picasso.get().load(url).into(imgAvatar);
        }
        rlProfileBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactsActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });


    }
    private void prepareMenu(ArrayList<NavItem> mNavItem){
        mNavItem.add(new NavItem(getString(R.string.emailhint), getString(R.string.emails_long), R.drawable.email));
        mNavItem.add(new NavItem(getString(R.string.folders), getString(R.string.folders_long), R.drawable.folders));
        mNavItem.add(new NavItem(getString(R.string.profile), getString(R.string.profile_long), R.drawable.person));
        mNavItem.add(new NavItem(getString(R.string.settings), getString(R.string.settings_long), R.drawable.settings));
        mNavItem.add(new NavItem(getString(R.string.logOut), getString(R.string.logoOut_long), R.mipmap.ic_action_about));
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectItemFromDrawer(i);
        }
    }

    private void selectItemFromDrawer(int position){
        Intent intent;
        if (position == 0) {
            intent = new Intent(ContactsActivity.this, EmailsActivity.class);
            startActivity(intent);
        }
        else if (position == 1){
            intent = new Intent(ContactsActivity.this, FoldersActivity.class);
            startActivity(intent);
        }
        else if (position == 2 ){
            intent = new Intent(ContactsActivity.this, ProfileActivity.class);
            startActivity(intent);
        }
        else if (position == 3){
            intent = new Intent(ContactsActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        else if (position == 4){
            SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putString(LoginActivity.USER_USERNAME, null);
            editor.putString(LoginActivity.USER_PASSWORD, null);
            editor.apply();
            intent = new Intent(ContactsActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else{
            Log.e("DRAWER", "Nesto van opsega");
        }
        mDrawerList.setItemChecked(position, true);
        if (position != 4){
            setTitle(mNavItem.get(position).getmTitle());
        }
        mDrawerLayout.closeDrawer(mDrawerPane);
    }
    Runnable mStatus = new Runnable() {
        @Override
        public void run() {
            try{
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String syncTime = sharedPreferences.getString("pref_sync_list", "0");
                mInterval = TimeUnit.MINUTES.toMillis(Integer.parseInt(syncTime));
                Toast.makeText(getApplicationContext(), "Syncing :D...", Toast.LENGTH_SHORT).show();

                ContactsService contactsService = ServiceUtils.getRetrofitInstance().create(ContactsService.class);
                Call<ArrayList<Contacts>> call = contactsService.getContacts();
                call.enqueue(new Callback<ArrayList<Contacts>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Contacts>> call, Response<ArrayList<Contacts>> response) {
                        ArrayList<Contacts> contacts = response.body();
                        ContactsAdapter adapter = new ContactsAdapter(ContactsActivity.this, R.layout.contacts_list, contacts);
                        contactView.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<ArrayList<Contacts>> call, Throwable t) {
                        System.out.println("Greska!");

                    }
                });
            }
            finally {
                mHendler.postDelayed(mStatus, mInterval);
            }
        }
    };
    void startRepeatingTask(){
        mStatus.run();
    }
    void stopRepeatingTask(){
        mHendler.removeCallbacks(mStatus);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)){
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRepeatingTask();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
    @Override
    protected void onResume(){
        super.onResume();
        startRepeatingTask();
    }


}