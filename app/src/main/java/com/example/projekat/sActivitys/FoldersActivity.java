package com.example.projekat.sActivitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.projekat.R;


import com.example.projekat.activitys.FolderActivity;
import com.example.projekat.activitys.LoginActivity;
import com.example.projekat.activitys.ProfileActivity;
import com.example.projekat.activitys.SettingsActivity;
import com.example.projekat.adapter.DrawerAdapter;
import com.example.projekat.adapter.FolderAdapter;
import com.example.projekat.createActivitys.CreateContactActivity;
import com.example.projekat.createActivitys.CreateFolderActivity;
import com.example.projekat.model.Contacts;
import com.example.projekat.model.Folder;
import com.example.projekat.model.NavItem;
import com.example.projekat.service.FolderService;
import com.example.projekat.service.ServiceUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoldersActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ListView contactView;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mTitle;
    private ArrayList<NavItem> mNavItem = new ArrayList<>();
    private ArrayList<Contacts> mContacts = new ArrayList<>();
    private String username;
    private String url;
    private long mInterval;
    private Handler mHendler;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        prepareMenu(mNavItem);

        mTitle = getTitle();
        mDrawerLayout = findViewById(R.id.DrawerLayout);
        mDrawerList = findViewById(R.id.navList);
        contactView = findViewById(R.id.mainContent);
        mDrawerPane = findViewById(R.id.DrawerPane);
        DrawerAdapter adapter = new DrawerAdapter(this, mNavItem);
        mDrawerLayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        mDrawerList.setOnItemClickListener(new FoldersActivity.DrawerItemClickListener());
        mDrawerList.setAdapter(adapter);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setIcon(R.mipmap.ic_launcher);
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ){

            public void onDrawerClosed(View view){
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
            public void onDrawerOpened(View drawerView){
                getSupportActionBar().setTitle("Projekat");
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        SharedPreferences preferences = getSharedPreferences(LoginActivity.LOGGED_USER, Context.MODE_PRIVATE);
        username = preferences.getString(LoginActivity.USER_USERNAME, "");
        url = preferences.getString(LoginActivity.USER_IMG, "");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FoldersActivity.this, CreateFolderActivity.class);
                startActivity(intent);
            }
        });

        RelativeLayout rlProfileBox = findViewById(R.id.profileBox);

        TextView txtUsername = rlProfileBox.findViewById(R.id.UserName);
        txtUsername.setText(username);

        ImageView imgAvatar = rlProfileBox.findViewById(R.id.avatar);
        if (url.trim().isEmpty()){
            Picasso.get().load(R.drawable.profile_image).into(imgAvatar);

        }else{
            Picasso.get().load(url).into(imgAvatar);
        }
        rlProfileBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FoldersActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });



    }
    private void prepareMenu(ArrayList<NavItem> mNavItem){
        mNavItem.add(new NavItem(getString(R.string.emailhint), getString(R.string.emails_long), R.drawable.email));
        mNavItem.add(new NavItem(getString(R.string.contacts), getString(R.string.contacts_long), R.drawable.contacts));
        mNavItem.add(new NavItem(getString(R.string.profile), getString(R.string.profile_long), R.drawable.person));
        mNavItem.add(new NavItem(getString(R.string.settings), getString(R.string.settings_long), R.drawable.settings));
        mNavItem.add(new NavItem(getString(R.string.logOut), getString(R.string.logoOut_long), R.mipmap.ic_action_about));
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectItemFromDrawer(i);
        }
    }

    private void selectItemFromDrawer(int position){
        Intent intent;
        if (position == 0) {
            intent = new Intent(FoldersActivity.this, EmailsActivity.class);
            startActivity(intent);
        }
        else if (position == 1){
            intent = new Intent(FoldersActivity.this, ContactsActivity.class);
            startActivity(intent);
        }
        else if (position == 2 ){
            intent = new Intent(FoldersActivity.this, ProfileActivity.class);
            startActivity(intent);
        }
        else if (position == 3){
            intent = new Intent(FoldersActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        else if (position == 4){
            SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putString(LoginActivity.USER_USERNAME, null);
            editor.putString(LoginActivity.USER_PASSWORD, null);
            editor.apply();
            intent = new Intent(FoldersActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else{
            Log.e("DRAWER", "Nesto van opsega");
        }
        mDrawerList.setItemChecked(position, true);
        if (position != 4){
            setTitle(mNavItem.get(position).getmTitle());
        }
        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.folders_menu, menu);
        return true;

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)){
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else{
            super.onBackPressed();
        }

    }



    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onRestart(){
        super.onRestart();
    }

    @Override
    public void onResume(){
        super.onResume();
        startRepeatingTask();
    }

    @Override
    public void onPause(){
        super.onPause();
        stopRepeatingTask();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String syncTimeStr = sharedPreferences.getString("pref_sync_list", "0");
                mInterval = TimeUnit.MINUTES.toMillis(Integer.parseInt(syncTimeStr));
                Toast.makeText(getApplicationContext(), "Syncing...", Toast.LENGTH_SHORT).show();

                FolderService folderService = ServiceUtils.getRetrofitInstance().create(FolderService.class);
                Call<ArrayList<Folder>> call = folderService.getFolder();
                call.enqueue(new Callback<ArrayList<Folder>>() {
                    @Override
                    public void onResponse(@NonNull Call<ArrayList<Folder>> call, @NonNull Response<ArrayList<Folder>> response) {

                        ArrayList<Folder> folders = response.body();
                        ListView listView = findViewById(R.id.listFolderFolders);
                        FolderAdapter adapter = new FolderAdapter(FoldersActivity.this, R.layout.item_folder, folders);
                        listView.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ArrayList<Folder>> call, @NonNull Throwable t) {
                        t.printStackTrace();
                    }
                });


            }finally {
                mHendler.postDelayed(mStatusChecker,mInterval);
            }
        }
    };

    void startRepeatingTask(){
        mStatusChecker.run();
    }

    void stopRepeatingTask(){
        mHendler.removeCallbacks(mStatusChecker);
    }
}