package com.example.projekat.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.projekat.R;
import com.example.projekat.activitys.ContactActivity;
import com.example.projekat.model.Contacts;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ContactsAdapter extends ArrayAdapter<Contacts> {

    private Context context;
    private int resource;
    private ArrayList<Contacts> contacts;

    public ContactsAdapter(Context context, int resource, ArrayList<Contacts> contacts){
        super(context,resource,contacts);

        this.context = context;
        this.resource = resource;
        this.contacts = contacts;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final Contacts c = contacts.get(position);

        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        }

        ImageView img = convertView.findViewById(R.id.image_icon);
        TextView txtIme = convertView.findViewById(R.id.firstname);
        TextView txtPrezime = convertView.findViewById(R.id.lastname);




        if(c.getDisplay() != null){
            String url = c.getDisplay().getPath() == null ? "": c.getDisplay().getPath();
            if(!url.isEmpty())
                Picasso.get().load(url).into(img);
            else
                Picasso.get().load(R.drawable.profile_image).into(img);
        }else {
            Picasso.get().load(R.drawable.profile_image).into(img);
        }

        txtIme.setText(c.getFirstName());
        txtPrezime.setText(c.getLastName());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent e = new Intent(context.getApplicationContext(), ContactActivity.class);
                e.putExtra("contact", c.getID());
                context.startActivity(e);
            }
        });

        return convertView;
    }
}