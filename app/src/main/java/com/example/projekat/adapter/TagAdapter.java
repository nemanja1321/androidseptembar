package com.example.projekat.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekat.R;
import com.example.projekat.model.Tag;

import java.util.ArrayList;
import java.util.Random;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.TagViewHolder> {

    private static final String TAG = "TagAdapter";

    private Context context;
    private ArrayList<Tag> tagoviEmaila;

    public TagAdapter(Context context, ArrayList<Tag> tagoviEmaila) {
        this.context = context;
        this.tagoviEmaila = tagoviEmaila;
    }

    @NonNull
    @Override
    public TagViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag, parent, false);
        return new TagViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull TagViewHolder tagViewHolder, int i) {
        Log.d(TAG, "onBindViewHolder: called.");

        tagViewHolder.tagName.setText(tagoviEmaila.get(i).getName());

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tagViewHolder.tagName.setBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return tagoviEmaila.size();
    }

    public class TagViewHolder extends RecyclerView.ViewHolder{

        TextView tagName;
        LinearLayout tagLayout;

        public TagViewHolder(@NonNull View itemView) {
            super(itemView);

            tagName = itemView.findViewById(R.id.txtTagName);
            tagLayout = itemView.findViewById(R.id.tagItem);
        }
    }

}
