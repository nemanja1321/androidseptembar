package com.example.projekat.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projekat.R;
import com.example.projekat.model.NavItem;

import java.util.ArrayList;

public class DrawerAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<NavItem> mNavItem;

    public DrawerAdapter(Context mContext, ArrayList<NavItem> mNavItem) {
        this.mContext = mContext;
        this.mNavItem = mNavItem;
    }

    @Override
    public int getCount() {
        return mNavItem.size();
    }

    @Override
    public Object getItem(int i) {
        return mNavItem.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.nav_header, null);
        }
        else{
            view = convertView;
        }
        TextView titleView = view.findViewById(R.id.title);
        TextView subtitleView = view.findViewById(R.id.subTitle);
        ImageView iconView = view.findViewById(R.id.icon);

        titleView.setText(mNavItem.get(i).getmTitle());
        subtitleView.setText(mNavItem.get(i).getmSubtitle());
        iconView.setImageResource(mNavItem.get(i).getmIcon());
        return view;
    }
}
