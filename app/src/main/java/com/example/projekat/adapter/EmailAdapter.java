package com.example.projekat.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekat.R;
import com.example.projekat.activitys.EmailActivity;
import com.example.projekat.model.Message;
import com.example.projekat.model.Tag;

import java.util.ArrayList;


public class EmailAdapter extends ArrayAdapter<Message> {
    private Context context;
    private int resource;
    private ArrayList<Message> messages;


    public EmailAdapter(Context context, int resource, ArrayList<Message> messages) {
        super(context,resource, messages);

        this.context = context;
        this.resource = resource;
        this.messages = messages;
    }


    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final Message m = messages.get(position);


        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        }


        ImageView img = convertView.findViewById(R.id.imgEmail);
        TextView txtEmail = convertView.findViewById(R.id.txtEmailAdresa);
        TextView txtSubject = convertView.findViewById(R.id.txtSubject);
        TextView txtContent = convertView.findViewById(R.id.txtContent);
        RecyclerView rvTags = convertView.findViewById(R.id.rvTagEmail);
//        TextView txtDatum = convertView.findViewById(R.id.txtDate);
//        TextView txtVreme = convertView.findViewById(R.id.txtTime);

        //postavljanje vrednosti u date view-ove
        //TODO promeniti na m.getFrom().getPhoto();
        img.setImageResource(R.drawable.profile_image);
        txtEmail.setText(m.getFrom().getEmail());

        String subject = m.getSubject();
        if (subject.length() > 20){
            subject = subject.substring(0, 17) + "...";
        }
        txtSubject.setText(subject);

        if(!m.isRead()){
            txtSubject.setTypeface(null, Typeface.BOLD);
        }

        String sadrzaj = m.getContent();
        if (sadrzaj.length() > 40){
            sadrzaj = sadrzaj.substring(0, 35) + "...";
        }
        txtContent.setText(sadrzaj);

        ArrayList<Tag> tags = m.getTags() != null ? m.getTags():new ArrayList<Tag>();
        TagAdapter adapter = new TagAdapter(context, tags);
        rvTags.setAdapter(adapter);
        rvTags.setLayoutManager(new LinearLayoutManager(context));

        //txtDatum.setText(getDate(m.getDateTime()));
        //txtVreme.setText(getTime(m.getDateTime()));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent e = new Intent(context.getApplicationContext(), EmailActivity.class);
                e.putExtra("messageID", m.getId());
                context.startActivity(e);
            }
        });

        return convertView;
    }
}
