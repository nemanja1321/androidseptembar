package com.example.projekat.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.projekat.R;
import com.example.projekat.activitys.FolderActivity;
import com.example.projekat.model.Contacts;
import com.example.projekat.model.Folder;

import java.util.ArrayList;

public class FolderAdapter extends ArrayAdapter<Folder> {
///////////////////////////////
    private Context context;
    private int resource;
    private ArrayList<Folder> folders;

    public FolderAdapter(Context context, int resource,  ArrayList<Folder> folders) {
        super(context, resource, folders);

        this.context = context;
        this.resource = resource;
        this.folders = folders;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final Folder f = folders.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        }

        ImageView img = convertView.findViewById(R.id.image_icon);
        TextView txtNaziv = convertView.findViewById(R.id.folderName);
        TextView txtBrojPoruka = convertView.findViewById(R.id.folderBrojPoruka);

        img.setImageResource(R.drawable.profile_image);
        txtNaziv.setText(f.getName());
        int size = f.getMessage().size();
        txtBrojPoruka.setText(String.valueOf(size));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent e = new Intent(context.getApplicationContext(), FolderActivity.class);
                e.putExtra("folder", f.getId());
                context.startActivity(e);
            }
        });

        return convertView;
    }

}
