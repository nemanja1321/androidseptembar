package com.example.projekat.model;



import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;


public class Contacts {

    @SerializedName("id")
    private int ID;

    @SerializedName("firstName")
    private String FirstName;

    @SerializedName("lastName")
    private String LastName;

    @SerializedName("display")
    private Photo Display;

    @SerializedName("email")
    private String Email;

    @SerializedName("format")
    private Format Format;

    private ArrayList<Message> From;
    private ArrayList<Message> To;
    private ArrayList<Message> Bcc;
    private ArrayList<Message> Cc;
///////////////////////////////
    public Contacts() {
    }

    public Contacts(int ID, String firstName, String lastName, Photo display, String email, Format format, ArrayList<Message> from, ArrayList<Message> to, ArrayList<Message> bcc, ArrayList<Message> cc) {
        this.ID = ID;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.Display = display;
        this.Email = email;
        this.Format = format;
        this.From = from;
        this.To = to;
        this.Bcc = bcc;
        this.Cc = cc;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public Photo getDisplay() {
        return Display;
    }

    public void setDisplay(Photo display) {
        Display = display;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Format getFormat() {
        return Format;
    }

    public void setFormat(Format format) {
        this.Format = format;
    }

    public ArrayList<Message> getFrom() {
        return From;
    }

    public void setFrom(ArrayList<Message> from) {
        From = from;
    }

    public ArrayList<Message> getTo() {
        return To;
    }

    public void setTo(ArrayList<Message> to) {
        To = to;
    }

    public ArrayList<Message> getBcc() {
        return Bcc;
    }

    public void setBcc(ArrayList<Message> bcc) {
        Bcc = bcc;
    }

    public ArrayList<Message> getCc() {
        return Cc;
    }

    public void setCc(ArrayList<Message> cc) {
        Cc = cc;
    }
}
