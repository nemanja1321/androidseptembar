package com.example.projekat.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Account implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("smtp")
    private String smtp;
    @SerializedName("pop3Imap")
    private String pop3Imap;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("message")
    private ArrayList<Message> message;
    @SerializedName("pop3")
    private String POP3;
    @SerializedName("image")
    private Photo Image;


    public Account(int id, String smtp, String pop3Imap, String username, String password, ArrayList<Message> message, String POP3, Photo image) {
        this.id = id;
        this.smtp = smtp;
        this.pop3Imap = pop3Imap;
        this.username = username;
        this.password = password;
        this.message = message;
        this.POP3 = POP3;
        Image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getPop3Imap() {
        return pop3Imap;
    }

    public void setPop3Imap(String pop3Imap) {
        this.pop3Imap = pop3Imap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Message> getMessage() {
        return message;
    }

    public void setMessage(ArrayList<Message> message) {
        this.message = message;
    }

    public String getPOP3() {
        return POP3;
    }

    public void setPOP3(String POP3) {
        this.POP3 = POP3;
    }

    public Photo getImage() {
        return Image;
    }

    public void setImage(Photo image) {
        Image = image;
    }
}
