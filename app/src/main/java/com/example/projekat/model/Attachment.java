package com.example.projekat.model;

import android.util.Base64;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class Attachment implements Serializable {

    @SerializedName("id")
    private int id;
    private Base64 data;
    private String type;
    private String name;
    private Message message;


    public Attachment(int id,Base64 data, String type, String name, Message message) {
        this.id = id;
        this.data = data;
        this.type = type;
        this.name = name;
        this.message= message;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

///////////////////////

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Base64 getData() {
        return data;
    }

    public void setData(Base64 data) {
        this.data = data;
    }
}
