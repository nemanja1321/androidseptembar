package com.example.projekat.model;




import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Message implements Serializable {



    @SerializedName("id")
    private int id;
    @SerializedName("from")
    private Contacts From;

    @SerializedName("to")
    private Contacts To;

    @SerializedName("cc")
    private ArrayList<Contacts> CC;

    @SerializedName("bcc")
    private ArrayList<Contacts> BCC;

//    @SerializedName("dateTime")
//    private Date DateTime;

    @SerializedName("subject")
    private String Subject;

    @SerializedName("content")
    private String Content;

    @SerializedName("prilog")
    private ArrayList<Attachment> Prilog;

    @SerializedName("tags")
    private ArrayList<Tag> Tags;

    @SerializedName("folder")
    private Folder Folder;

    @SerializedName("procitana")
    private boolean Procitana = true;

    public Message() {
    }

    public Message(int id, Contacts from, Contacts to, ArrayList<Contacts> CC, ArrayList<Contacts> BCC/*, Date dateTime*/, String subject, String content, ArrayList<Attachment> prilog, ArrayList<Tag> tags, Folder folder) {
        this.id = id;
        this.From = from;
        this.To = to;
        if(CC != null){
            this.setCC(CC);
        }
        this.CC = CC;
        if(BCC != null){
            this.setBCC(BCC);
        }
        this.BCC = BCC;
//        this.DateTime = dateTime;
        this.Subject = subject;
        this.Content = content;
        this.Prilog = prilog;
        this.Tags = tags;
        this.Folder = folder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contacts getFrom() {
        return From;
    }

    public void setFrom(Contacts from) {
        From = from;
    }

    public Contacts getTo() {
        return To;
    }

    public void setTo(Contacts to) {
        To = to;
    }

    public ArrayList<Contacts> getCC() {
        return CC;
    }

    public void setCC(ArrayList<Contacts> CC) {
        if (CC != null)
            this.setFrom(CC.get(0));
        this.CC = CC;
    }
////////
    public ArrayList<Contacts> getBCC() {
        return BCC;
    }

    public void setBCC(ArrayList<Contacts> BCC) {
        if (BCC != null)
            this.setFrom(BCC.get(0));
        this.BCC = BCC;
    }

//    public Date getDateTime() {
//        return DateTime;
//    }
//
//    public void setDateTime(Date dateTime) {
//        DateTime = dateTime;
//    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public ArrayList<Attachment> getPrilog() {
        return Prilog;
    }

    public void setPrilog(ArrayList<Attachment> prilog) {
        Prilog = prilog;
    }

    public ArrayList<Tag> getTags() {
        return Tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        Tags = tags;
    }

    public Folder getFolder() {
        return Folder;
    }

    public void setFolder(Folder folder) {
        Folder = folder;
    }

    public boolean isRead() {
        return Procitana;
    }

    public void setRead(boolean read) {
        Procitana = read;
    }








}
