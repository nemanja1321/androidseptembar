package com.example.projekat.model;

public enum Condition {

    TO,
    FROM,
    CC,
    SUBJECT;

    public static Condition getCondition(String a) {
        switch (a) {
            case "FROM":
                return FROM;
            case "CC":
                return CC;
            case "SUBJECT":
                return SUBJECT;
            default:
                return TO;
        }
    }
//////////////////////////
    public static int toInt(Condition k) {
        switch (k) {
            case FROM:
                return 2;
            case CC:
                return 3;
            case SUBJECT:
                return 4;
            default:
                return 1;
        }
    }
}
