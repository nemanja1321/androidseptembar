package com.example.projekat.model;

public enum Operation {


    MOVE,
    COPY,
    DELETE;

    public static Operation getOperation(String a) {
        switch (a) {
            case "COPY":
                return COPY;
            case "DELETE":
                return DELETE;
            default:
                return MOVE;
        }
    }
/////////
    public static int toInt(Operation k) {
        switch (k) {
            case COPY:
                return 2;
            case DELETE:
                return 3;
            default:
                return 1;
        }
    }
}
