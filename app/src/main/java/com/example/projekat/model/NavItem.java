package com.example.projekat.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NavItem implements Parcelable {
    private String mTitle;
    private String mSubtitle;
    private int mIcon;

    protected NavItem(Parcel in) {
        mTitle = in.readString();
        mSubtitle = in.readString();
        mIcon = in.readInt();
    }

    public NavItem(String mTitle, String mSubtitle, int mIcon) {
        this.mTitle = mTitle;
        this.mSubtitle = mSubtitle;
        this.mIcon = mIcon;
    }

    @Override
    public String toString() {
        return "NavItem{" +
                "mTitle='" + mTitle + '\'' +
                '}';
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmSubtitle() {
        return mSubtitle;
    }

    public void setmSubtitle(String mSubtitle) {
        this.mSubtitle = mSubtitle;
    }

    public int getmIcon() {
        return mIcon;
    }

    public void setmIcon(int mIcon) {
        this.mIcon = mIcon;
    }

    public static final Creator<NavItem> CREATOR = new Creator<NavItem>() {
        @Override
        public NavItem createFromParcel(Parcel in) {
            return new NavItem(in);
        }

        @Override
        public NavItem[] newArray(int size) {
            return new NavItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mSubtitle);
        dest.writeInt(mIcon);
    }


}
