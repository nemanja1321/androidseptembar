package com.example.projekat.model;

import android.os.Parcelable;
import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class Folder implements Serializable {

    @SerializedName("id")
    private int id;

    private String name;
/////////
    @SerializedName("message")
    private ArrayList<Message> message;

    @SerializedName("folders")
    private ArrayList<Folder> folders;

    @SerializedName("rules")
    private ArrayList<Rule> Rules;

    public Folder() {
    }

    public Folder(int id, String name, ArrayList<Message> message, ArrayList<Folder> folders, ArrayList<Rule> rules) {
        this.id = id;
        this.name = name;
        this.message = message;
        this.folders = folders;
        Rules = rules;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Message> getMessage() {
        return message;
    }

    public void setMessage(ArrayList<Message> message) {
        this.message = message;
    }

    public ArrayList<Folder> getFolders() {
        return folders;
    }

    public void setFolders(ArrayList<Folder> folders) {
        this.folders = folders;
    }

    public ArrayList<Rule> getRules() {
        return Rules;
    }

    public void setRules(ArrayList<Rule> rules) {
        Rules = rules;
    }
}
