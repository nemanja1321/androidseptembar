package com.example.projekat.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Photo implements Serializable {

    @SerializedName("id")
    private int ID;

    @SerializedName("path")
    private String Path;

    private Contacts Contact;

    public Photo() {
    }////////

    public Photo(int ID, String path, Contacts contact) {
        this.ID = ID;
        this.Path = path;
        this.Contact = contact;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public Contacts getContact() {
        return Contact;
    }

    public void setContact(Contacts contact) {
        Contact = contact;
    }
}
