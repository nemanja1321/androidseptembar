package com.example.projekat.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;////

public class Tag implements Serializable {


    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("messages")
    private ArrayList<Message> message;


    public Tag(int id, String name, ArrayList<Message> message) {
        this.id = id;
        this.name = name;
        this.message=message;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Message> getMessage() {
        return message;
    }

    public void setMessage(ArrayList<Message> message) {
        this.message = message;
    }
}
///////