package com.example.projekat.service;

import com.example.projekat.model.Contacts;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ContactsService {

    @GET("contacts/all")
    Call<ArrayList<Contacts>> getContacts();

    @GET("contacts/{id}")
    Call<Contacts> getByID(@Path("id") int contactID);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("contacts/add")
    Call<RequestBody> addContacts(@Body Contacts contacts);

    @Headers({
            "Content-Type:application/json"
    })
    @PUT("contacts/edit")
    Call<RequestBody> updateContact(@Body Contacts contacts);

    @DELETE("contact/delete/{id}")
    Call<RequestBody> deleteContact(@Path("id") int contactID);
}
