package com.example.projekat.service;

import com.example.projekat.model.Message;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MessageService {

    @GET("message/all")
    Call<ArrayList<Message>> getMessage(/*@Query("sort") boolean sort*/);

    @GET("message/{id}")
    Call<Message> getByID(@Path("id") int messageID);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("message/send")
    Call<RequestBody> send(@Body Message message);

    @DELETE("message/delete/{id}")
    Call<RequestBody> delete(@Path("id") int id);


}
