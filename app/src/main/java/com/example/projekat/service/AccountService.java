package com.example.projekat.service;

import com.example.projekat.model.Account;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AccountService {

    @GET("accounts/loginUser/{username}/{password}")
    Call<Account> login(@Path("username") String username, @Path("password") String password);

    @Headers({
            "Content-Type:application/json"
    })
    @PUT("account/edit")
    Call<Account> editAccount(@Body Account account);
}
