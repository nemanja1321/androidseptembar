package com.example.projekat.service;

import com.example.projekat.model.Folder;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FolderService {

    @GET("folder/all")
    Call<ArrayList<Folder>> getFolder();

    @GET("folder/{id}")
    Call<Folder> getByID(@Path("id") int folderID);

    @Headers({
            "Content-Type:application/json"
    })
    @POST("folder/add")
    Call<RequestBody> addFolder(@Body Folder folder);

    @Headers({
            "Content-Type:application/json"
    })
    @PUT("folder/edit")
    Call<RequestBody> editFolder(@Body Folder folder);

    @DELETE("folder/delete/{id}")
    Call<RequestBody> deleteFolder(@Path("id") int id);
}
