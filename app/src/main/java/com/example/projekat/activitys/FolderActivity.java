package com.example.projekat.activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.example.projekat.R;
import com.example.projekat.adapter.EmailAdapter;
import com.example.projekat.adapter.FolderAdapter;
import com.example.projekat.model.Folder;
import com.example.projekat.model.Message;
import com.example.projekat.sActivitys.EmailsActivity;
import com.example.projekat.service.FolderService;
import com.example.projekat.service.ServiceUtils;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FolderActivity extends AppCompatActivity {

    private EditText txtName;
    private Folder folder;
    private String name;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);

        Toolbar toolbar = findViewById(R.id.toolbar_folder);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.folder_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.home)
            finish();

        switch (item.getItemId()) {
            case R.id.saveFolder:
                LayoutInflater inflater = this.getLayoutInflater();
                View view = inflater.inflate(R.layout.dialog_folder_rename, null);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle("Edit folder name");
                alertDialog.setView(view);
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        txtName = findViewById(R.id.editFolderName);
                        name =   txtName.getText().toString().trim();
                    }
                });
                alertDialog.show();

                if(name != null){
                    Toast.makeText(getApplicationContext(), "Pogresan unos!", Toast.LENGTH_SHORT).show();
                }else{
                    folder.setName(name);
                    FolderService folderService = ServiceUtils.getRetrofitInstance().create(FolderService.class);
                    Call<RequestBody> call = folderService.editFolder(folder);
                    call.enqueue(new Callback<RequestBody>() {
                        @Override
                        public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {
                            if(response.isSuccessful()){
                                Toast.makeText(FolderActivity.this, R.string.save,Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), EmailsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onFailure(Call<RequestBody> call, Throwable t) {
                            Toast.makeText(FolderActivity.this, R.string.edit, Toast.LENGTH_SHORT).show();
                            System.out.println(t);
                        }
                    });
                }
                break;
            case R.id.deleteFolder:
                FolderService folderService = ServiceUtils.getRetrofitInstance().create(FolderService.class);
                Call<RequestBody> call = folderService.deleteFolder(folder.getId());
                call.enqueue(new Callback<RequestBody>() {
                    @Override
                    public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(getApplicationContext(), "Obrisano", Toast.LENGTH_SHORT);
                            Intent intent = new Intent(getApplicationContext(), EmailsActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestBody> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

                break;
            case R.id.cancelFolder:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onRestart(){
        super.onRestart();
    }

    @Override
    public void onResume(){
        super.onResume();

        Intent i = getIntent();
        Bundle b = i.getExtras();

        int id = b != null ? (int) b.get("folder"): -1;

        final FolderService folderService = ServiceUtils.getRetrofitInstance().create(FolderService.class);
        Call<Folder> call = folderService.getByID(id);
        call.enqueue(new Callback<Folder>() {
            @Override
            public void onResponse(@NonNull Call<Folder> call, @NonNull Response<Folder> response) {

                folder = response.body();
                if (folder != null){

                    ListView listaFoldera = findViewById(R.id.listFolderFolders);
                    ArrayList<Folder> folders= folder.getFolders() != null ? folder.getFolders(): new ArrayList<Folder>();
                    FolderAdapter folderAdapter = new FolderAdapter(getApplicationContext(), R.layout.item_folder, folders);
                    listaFoldera.setAdapter(folderAdapter);

                    ListView listView = findViewById(R.id.listMessageFolders);
                    ArrayList<Message> messsages = folder.getMessage()!= null ? folder.getMessage(): new ArrayList<Message>();
                    EmailAdapter adapter = new EmailAdapter(FolderActivity.this, R.layout.item_email, messsages);
                    listView.setAdapter(adapter);

                    FolderActivity.this.setTitle(folder.getName());
                }

            }

            @Override
            public void onFailure(@NonNull Call<Folder> call, @NonNull Throwable t) {
                Toast.makeText(FolderActivity.this, R.string.errKon, Toast.LENGTH_LONG).show();
                System.out.println(t);

            }


        });
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

}