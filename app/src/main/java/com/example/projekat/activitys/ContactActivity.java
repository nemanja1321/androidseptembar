package com.example.projekat.activitys;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.content.res.Configuration;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projekat.R;
import com.example.projekat.adapter.DrawerAdapter;
import com.example.projekat.model.Contacts;
import com.example.projekat.model.NavItem;
import com.example.projekat.sActivitys.ContactsActivity;
import com.example.projekat.sActivitys.EmailsActivity;
import com.example.projekat.sActivitys.FoldersActivity;
import com.example.projekat.service.ContactsService;
import com.example.projekat.service.ServiceUtils;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity {

    private EditText editName;
    private EditText editLast;
    private EditText editEmail;
    private TextView editFormat;
    private ImageView imgAvatar;
    private Contacts contacts;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);



        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        final ActionBar actionBar = getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setIcon(R.mipmap.ic_launcher);
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }
        editName = findViewById(R.id.tvIme);
        editLast = findViewById(R.id.tvPrezime);
        editEmail = findViewById(R.id.tvEmail);
        imgAvatar = findViewById(R.id.imageView);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contact_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.home){
            finish();
        }
        if (item.getItemId() == R.id.saveContact){
            String firstName = editName.getText().toString().trim();
            String lastName = editLast.getText().toString().trim();
            String email = editEmail.getText().toString().trim();

            if (firstName.equals("") || lastName.equals("") || email.equals("")){
                Toast.makeText(getApplicationContext(), "Pogresan unos!", Toast.LENGTH_SHORT).show();
            }
            else{
                contacts.setFirstName(firstName);
                contacts.setLastName(lastName);
                contacts.setEmail(email);
                ContactsService contactsService = ServiceUtils.getRetrofitInstance().create(ContactsService.class);
                Call<RequestBody> call = contactsService.updateContact(contacts);
                call.enqueue(new Callback<RequestBody>() {
                    @Override
                    public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {
                        if (response.isSuccessful()){
                            Toast.makeText(ContactActivity.this, R.string.save, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), EmailsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestBody> call, Throwable t) {
                        Toast.makeText(ContactActivity.this, "Greska!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }
        else if (item.getItemId() == R.id.cancelContact){
            super.onBackPressed();
        }
        else if (item.getItemId() == R.id.deleteContact){
            ContactsService contactsService = ServiceUtils.getRetrofitInstance().create(ContactsService.class);
            Call<RequestBody> call = contactsService.deleteContact(contacts.getID());
            call.enqueue(new Callback<RequestBody>() {
                @Override
                public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {
                    if (response.isSuccessful()){
                        Toast.makeText(ContactActivity.this, "Uspeh!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), EmailsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<RequestBody> call, Throwable t) {
                    Toast.makeText(ContactActivity.this, "Greska!", Toast.LENGTH_SHORT).show();
                }
            });
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
    }
    @Override
    protected void onResume(){
        super.onResume();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        int id = bundle !=null ? (int) bundle.get("contact") : -1;

        final ContactsService contactsService = ServiceUtils.getRetrofitInstance().create(ContactsService.class);
        Call<Contacts> call = contactsService.getByID(id);
        call.enqueue(new Callback<Contacts>() {
            @Override
            public void onResponse(Call<Contacts> call, Response<Contacts> response) {
                contacts = response.body();
                if (contacts != null){
                    editName = findViewById(R.id.tvIme);
                    editName.setText(contacts.getFirstName());
                    editLast = findViewById(R.id.tvPrezime);
                    editLast.setText(contacts.getLastName());
                    editEmail = findViewById(R.id.tvEmail);
                    editEmail.setText(contacts.getEmail());
                    editFormat = findViewById(R.id.editFormat);
                    editFormat.setText(contacts.getFormat().toString());
                }
            }

            @Override
            public void onFailure(Call<Contacts> call, Throwable t) {
                Toast.makeText(ContactActivity.this, "Proglem sa konekcijom, probajte kasnije", Toast.LENGTH_SHORT).show();
            }
        });
    }




}