package com.example.projekat.activitys;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projekat.R;
import com.example.projekat.adapter.DrawerAdapter;
import com.example.projekat.model.Account;
import com.example.projekat.model.NavItem;
import com.example.projekat.sActivitys.EmailsActivity;
import com.example.projekat.sActivitys.FoldersActivity;
import com.example.projekat.service.AccountService;
import com.example.projekat.service.ServiceUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private DrawerLayout mDrawerlayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mTitle;
    private ArrayList<NavItem> mNavItem = new ArrayList<>();

    private TextView txtUsername;
    private TextView txtPassword;

    private static final String LOGGED_USER = "LOGGED_USER";
    SharedPreferences logged;
    private SharedPreferences.Editor editor;
    private Account account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        prepareMenu(mNavItem);

        mTitle = getTitle();
        mDrawerlayout = findViewById(R.id.DrawerLayout2);
        mDrawerList = findViewById(R.id.navList2);
        mDrawerPane = findViewById(R.id.drawerPane2);
        DrawerAdapter adapter = new DrawerAdapter(this, mNavItem);
        mDrawerlayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerList.setAdapter(adapter);

        Toolbar toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setIcon(R.mipmap.ic_launcher);
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerlayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ){
            public void onDrawerOpened(View draverView){
                getSupportActionBar().setTitle("Projekat");
                invalidateOptionsMenu();
            }
            public void onDrawerClosed(View view){
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
        };

        logged = getSharedPreferences(LOGGED_USER, Context.MODE_PRIVATE);
        editor = logged.edit();


    }
    private void prepareMenu(ArrayList<NavItem> mNavItem){
        mNavItem.add(new NavItem(getString(R.string.contacts), getString(R.string.contacts_long), R.drawable.contacts));
        mNavItem.add(new NavItem(getString(R.string.emails), getString(R.string.emails_long), R.drawable.email));
        mNavItem.add(new NavItem(getString(R.string.folders), getString(R.string.folders_long), R.drawable.folders));
        mNavItem.add(new NavItem(getString(R.string.settings), getString(R.string.settings_long), R.drawable.settings));
        mNavItem.add(new NavItem(getString(R.string.logOut), getString(R.string.logoOut_long), R.mipmap.ic_action_about));
    }
    private class DrawerItemClickListener implements ListView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            selectItemFromDrawer(i);
        }
    }
    private void selectItemFromDrawer(int position){
        Intent intent;
        if (position == 0){
            intent = new Intent(ProfileActivity.this, ContactActivity.class);
            startActivity(intent);
        }
        else if (position == 1){
            intent = new Intent(ProfileActivity.this, EmailsActivity.class);
            startActivity(intent);
        }
        else if (position == 2){
            intent = new Intent(ProfileActivity.this, FoldersActivity.class);
            startActivity(intent);
        }
        else if (position == 3){
            intent = new Intent(ProfileActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        else if (position == 4){
            intent = new Intent(ProfileActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        else {
            Log.e("DRAWER", "Nesto van opsega");
        }
        mDrawerList.setItemChecked(position, true);
        if (position != 4){
            setTitle(mNavItem.get(position).getmTitle());
        }
        mDrawerlayout.closeDrawer(mDrawerPane);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        final Button edit = findViewById(R.id.editButton);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = txtUsername.getText().toString().trim();
                String password = txtPassword.getText().toString().trim();

                if (!username.equals("") && !password.equals("")){
                    account.setUsername(username);
                    account.setPassword(password);

                    final AccountService accountService = ServiceUtils.getRetrofitInstance().create(AccountService.class);
                    Call<Account> call = accountService.editAccount(account);
                    call.enqueue(new Callback<Account>() {
                        @Override
                        public void onResponse(Call<Account> call, Response<Account> response) {
                            account = response.body();
                            editor.putString(LoginActivity.USER_PASSWORD, null);
                            editor.putString(LoginActivity.USER_USERNAME, account.getUsername());
                            editor.apply();

                            Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<Account> call, Throwable t) {
                            Toast.makeText(ProfileActivity.this, "Problem sa konekciojom, probajte ponovo!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onResume(){
        super.onResume();

        String username = logged.getString(LoginActivity.USER_USERNAME, null);
        String password = logged.getString(LoginActivity.USER_PASSWORD, null);

        AccountService accountService = ServiceUtils.getRetrofitInstance().create(AccountService.class);
        Call<Account> call = accountService.login(username, password);
        call.enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Call<Account> call, Response<Account> response) {
                account = response.body();
                if (account != null){
                    txtUsername = findViewById(R.id.txtUsernameProfile);
                    txtPassword = findViewById(R.id.txtPasswordProfile);
                }
            }

            @Override
            public void onFailure(Call<Account> call, Throwable t) {
                Toast.makeText(ProfileActivity.this, "Problem sa konekcijom, probajte opet!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
