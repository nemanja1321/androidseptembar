package com.example.projekat.activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import com.example.projekat.R;

import com.example.projekat.model.Message;
import com.example.projekat.sActivitys.EmailsActivity;
import com.example.projekat.service.MessageService;
import com.example.projekat.service.ServiceUtils;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;/////

public class EmailActivity extends AppCompatActivity {
    private Message message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);


        Toolbar toolbar = findViewById(R.id.toolbar_email);
        setSupportActionBar(toolbar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.email_menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.home)
            finish();

        switch (item.getItemId()) {
            case R.id.save:
                Toast.makeText(EmailActivity.this, R.string.save,Toast.LENGTH_SHORT).show();
                break;
            case R.id.delete:
                MessageService messageService = ServiceUtils.getRetrofitInstance().create(MessageService.class);
                Call<RequestBody> call = messageService.delete(message.getId());
                call.enqueue(new Callback<RequestBody>() {
                    @Override
                    public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(EmailActivity.this, R.string.delete,Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), EmailsActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestBody> call, Throwable t) {
                        Toast.makeText(EmailActivity.this, R.string.E404,Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                    }
                });

                break;
            case R.id.reply:
                Toast.makeText(EmailActivity.this, R.string.reply,Toast.LENGTH_SHORT).show();
                break;
            case R.id.forward:
                Toast.makeText(EmailActivity.this, R.string.forward,Toast.LENGTH_SHORT).show();
                break;
            case R.id.replyToAll:
                Toast.makeText(EmailActivity.this, R.string.replyToAll,Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);


    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onResume(){
        super.onResume();

        Intent i = getIntent();
        Bundle b = i.getExtras();

        int messageID = b != null ? (int) b.get("messageID"): -1;
        MessageService messageService = ServiceUtils.getRetrofitInstance().create(MessageService.class);
        Call<Message> call = messageService.getByID(messageID);
        call.enqueue(new Callback<Message>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<Message> call, @NonNull Response<Message> response) {

                message = response.body();

                TextView twEmail = findViewById(R.id.EmailFrom);
                twEmail.setText(message.getFrom().getEmail());

                TextView twText = findViewById(R.id.EmailText);
                twText.setText(message.getContent());

                TextView twSubject = findViewById(R.id.EmailSubject);
                twSubject.setText(message.getSubject());

//                TextView twDate = findViewById(R.id.lblEmailDate);
//                twDate.setText(DateOperation.getDateTime(message.getDateTime()));

                TextView txtAtt = findViewById(R.id.txtAttachment);
                txtAtt.setText("Attachment: " + message.getPrilog().size());

            }

            @Override
            public void onFailure(@NonNull Call<Message> call, @NonNull Throwable t) {
                System.out.println("Error!");
            }
        });
    }


}

////////////////////////