package com.example.projekat.activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projekat.R;
import com.example.projekat.model.Account;
import com.example.projekat.sActivitys.EmailsActivity;
import com.example.projekat.service.AccountService;
import com.example.projekat.service.ServiceUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    public static  final  String LOGGED_USER = "LOGGED_USER";
    public static final String USER_USERNAME = "USERNAME";
    public static final String USER_PASSWORD = "PASSWORD";
    public static final String USER_IMG = "IMG_URL";
    private TextView txtUsername;
    private TextView txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = findViewById(R.id.username);
        txtPassword = findViewById(R.id.password);

        SharedPreferences logged = getSharedPreferences(LOGGED_USER, Context.MODE_PRIVATE);
        String username = logged.getString(USER_USERNAME, null);

        if (username != null){
            txtUsername.setText(username);
        }
    }





    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences logged  = getSharedPreferences(LOGGED_USER, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = logged.edit();

        final String username = txtUsername.getText().toString().trim();
        final String password = txtPassword.getText().toString().trim();

        Button logInBtn = findViewById(R.id.loginBtn);
        logInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = txtUsername.getText().toString().trim();
                String password = txtPassword.getText().toString().trim();

                if (username.equals("") && password.equals("")){
                    Toast.makeText(LoginActivity.this, "Niste nista uneli, probajte ponovo.", Toast.LENGTH_SHORT).show();
                }
                else{
                    AccountService accountService = ServiceUtils.getRetrofitInstance().create(AccountService.class);
                    Call<Account> call = accountService.login(username,password);
                    call.enqueue(new Callback<Account>() {
                        @Override
                        public void onResponse(Call<Account> call, Response<Account> response) {
                            Account account = response.body();

                            if (account != null){
                                editor.putString(USER_USERNAME, account.getUsername());
                                editor.putString(USER_PASSWORD, account.getPassword());
                                editor.putString(USER_IMG, account.getImage().getPath());
                                editor.apply();
                                Intent intent = new Intent(LoginActivity.this, EmailsActivity.class);
                                startActivity(intent);
                            }
                            else{
                                Toast.makeText(LoginActivity.this, "Pokresan unos, probajte novo!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Account> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, "Problem sa konekcijom, probajte malo kasnije.", Toast.LENGTH_SHORT).show();


                        }
                    });
                }
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    protected void onResume(){
        super.onResume();
    }

}