package com.example.projekat.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.example.projekat.R;

public class SettingsActivity extends PreferenceActivity {

    private String synctime;
    private boolean allowSync;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }


    private void consultPreferences(){

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        synctime = sharedPreferences.getString(getString(R.string.pref_sync_list), "1");// pola minuta
        allowSync = sharedPreferences.getBoolean(getString(R.string.pref_sync), false);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    protected void onResume(){
        super.onResume();
    }

}